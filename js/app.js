const clear = document.querySelector(".clear");
const dateElement = document.getElementById("date");
const list = document.getElementById("list");
const input = document.getElementById("input")



const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";

//variable
let LIST , id ;


let data = localStorage.getItem("TODO");

if(data){
LIST = JSON.parse(data);
id = LIST.length;
loadList(LIST);

}else{
LIST = [];
id = 0;
}


function loadList(array){
    array.forEach(function(item){
        addToDo(item.name, item.id, item.done, item.trash);
    }); 
}
clear.addEventListener("click", function(){
    localStorage.clear();
    location.reload();
});

// Date display
const options = {weekday : "long", month:"short", day:"numeric"};  
const today = new Date();

dateElement.innerHTML = today.toLocaleDateString("en-US", options)

// add to do functon

function addToDo(toDo, id, done, trash){
if(trash){return;}

const DONE  = done ? CHECK : UNCHECK;
const LINE = done  ? LINE_THROUGH : "";

const item =
            `<li class="item">
            <i class="fa  ${DONE} co" job="complete" id="${id}"></i>
                <p class="text ${LINE}">${toDo}</p>
                <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                </li>
                `;
    const position = "beforeend";
    list.insertAdjacentHTML(position, item);
}



// add enter function
document.addEventListener("keyup", function(even){
    if(event.keyCode == 13){
const toDo = input.value;

// if it isint empty
if(toDo){
addToDo(toDo, id, false, false);

LIST.push({ 
    name : toDo,
    id : id,
    done : false,
    trash : false
    
});

 
localStorage.setItem("TODO", JSON.stringify(LIST));
id++;
}
input.value = "";
    }
});



function completeToDo(element){
    element.classList.toggle(CHECK);
    element.classList.toggle(UNCHECK);
    element.parentNode.querySelector(".text"). classList.toggle(LINE_THROUGH);

    LIST[element.id].done = LIST[element.id].done ? false : true;
}



function removeToDo(element){
    element.parentNode.parentNode.removeChild(element.parentNode);

    LIST[element.id].trash = true;
}

list.addEventListener("click", function(event){
    const element = event.target;
    const elementJob = element.attributes.job.value;

    if(elementJob == "complete"){
completeToDo(element)
    }else if(elementJob =="delete"){
removeToDo(element); 
    }

     
localStorage.setItem("TODO", JSON.stringify(LIST));
});











// display = document.getElementById('list');
// let i = document.getElementById('input');



// i.addEventListener('blur',function(e){
//     e.preventDefault();
//   let inpuValue=this.value;
// li= document.createElement('li');
// text= document.createTextNode(inpuValue);
// li.append(text);
// display.appendChild(li);
// this.value='';
// })






// let f =0;
// let names = [] 

// function toDo(){

//      let x;
// let a = "<p/>"
//  names[f] = document.getElementById("input").value
// //  times = document.getElementById('time').value

//  f++

//  for(x = 0; x<names.length; x++){
//   console.log(names)
//    a +=names[x];

//  }
//  document.getElementById('list').innerHTML = a;

// }
